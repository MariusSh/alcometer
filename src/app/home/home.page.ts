import { Component, OnInit } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  gender: string;
  weight: number;
  time: number;
  bottles: number;
  promilles: number;

  ngOnInit() {

    this.gender = "male" ;
    this.weight = 84;

  }

  constructor() {

    this.weight = 0;
    this.time = 0;
    this.bottles = 0;
    this.promilles = 0;

  }

  calculate(){

    var litres = this.bottles * 0.33
    var grams = litres * 8 * 4.5
    var burning = this.weight / 10
    var grams_left = grams - (burning * this.time)

    if (this.gender === "male" ) {
      
      this.promilles = grams_left / (this.weight * 0.7);

    } else {
      
      this.promilles = grams_left / (this.weight * 0.6);

    }
    
    if (this.promilles<0) {

      this.promilles=0

    }
   
  }

}